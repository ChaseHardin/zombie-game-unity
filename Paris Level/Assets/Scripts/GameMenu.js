﻿var play = false;  
var controls = false; 
var credits = false; 
var quit = false; 
var textSize : int = 45; 


function Start() 
{
    guiText.alignment = TextAlignment.Center; 
    
    guiText.fontSize = textSize; 
    
}

function OnMouseEnter(){
    //change text size
    guiText.fontSize = textSize+5;
}

function OnMouseExit() 
{
    guiText.fontSize = textSize; 
}

function OnMouseUp(){
   
    if(play == true) {
    //load level
    Application.LoadLevel("Paris Scene");
    }
     
    if(controls)
    {
        Application.LoadLevel("GameControlPage");
    }
    
    if(credits)
    {
       // Application.LoadLevel("Instructions");
    }
       //is this quit
    if (quit==true) {
    //quit the game
    Application.Quit();
    }
    
}

function Update(){
//quit game if escape key is pressed
if (Input.GetKey(KeyCode.Escape)) { Application.Quit();
}
}