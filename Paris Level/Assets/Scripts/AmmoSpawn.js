﻿var speed : float = 30f;
var waitTime : int = 30;
var gone : boolean = false;
var ammoPrefab : GameObject; 

function Start()
{    
    Instantiate(ammoPrefab, transform.position, transform.rotation);  
}

function Update () 
{    
    if(gone == true) 
    {
        Spawn(); 
    }
}
  
 function Spawn() 
{
    yield WaitForSeconds(waitTime); 
    Instantiate(ammoPrefab, transform.position, transform.rotation);  
}
