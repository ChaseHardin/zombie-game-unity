﻿var GetHealth : boolean; 
var player : GameObject; 
var speed : float = 30f; 
var findFirstAid : GameObject; 
private var guiHealth : GameObject;

function Start()
{
	player = GameObject.Find("Player");
	guiHealth = GameObject.Find("HealthBar");
	findFirstAid = GameObject.FindGameObjectWithTag("FirstAidSpawn"); 

}

function Update () 
{
	transform.Rotate(Vector3.right, speed * Time.deltaTime); 
}
function OnTriggerEnter(other : Collider) {
	if(other.tag == "Player") 
	{
		GetHealth = true; 
		player.SendMessage("AddHealth");
		//Debug.Log(player); 
		guiHealth.SendMessage("AddHealthWidth"); 
		Destroy(gameObject);
		findFirstAid.SendMessage("Spawn"); 

	}
}
  