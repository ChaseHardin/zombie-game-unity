﻿//Enemy's Health
var health = 3;
var takeDamage : boolean = false;
var playerDistance : int; //distance measurement
var healthimage : Texture2D; //Wound Image
var player : GameObject; 
var attacking : boolean; //Zombie attacking player
var zombieDead : boolean = false; 

private var healthBarScript: HealthBarScript; // health bar 
private var guiHealth : GameObject;


function Start() 
{
    player = GameObject.Find("Player"); 
    attacking = false;
    
    guiHealth = GameObject.Find("HealthBar");
    healthBarScript = guiHealth.GetComponent("HealthBarScript");
 }

function OnTriggerEnter(other : Collider){
    if(other.tag == "Player"){
        takeDamage = true;
    }
}

function OnTriggerExit(other : Collider){
    if(other.tag == "Player"){
        takeDamage = false;
    }
}

function Update(){
    if (takeDamage){
        if(Input.GetButtonDown("Fire1")){
            health--;
        }
    }
    
    if(health <- 0){
        //Debug.Log("Enemy Down!");
        health = 0;
        Destroy (gameObject);
    }
    
    playerDistance = Vector3.Distance(player.transform.position, transform.position);     
    if(playerDistance <=3) 
    {
        if((!attacking) && (!zombieDead))
        {
            Invoke("ApplyDamage", 3); 
            attacking = true; 
            reduceHealth(); 
        }
    } 
}

function SetZombieDead()
{
    zombieDead = true; 
}

function OnGUI() {
    var centeredStyle = GUI.skin.GetStyle("Label"); 
    centeredStyle.alignment = TextAnchor.UpperCenter; 
    if(attacking){
        GUI.Label(Rect(Screen.width/2-150, Screen.height/2-150,300,300),healthimage);
    }
}

function ApplyDamage(){
    player.SendMessage("SubtractHealth"); 
    attacking = false; 
}

function reduceHealth() {
    if(healthBarScript.healthWidth >= 0) {
           healthBarScript.healthWidth = healthBarScript.healthWidth - 20;
    }
}