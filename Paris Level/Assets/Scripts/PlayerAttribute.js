﻿//Player Attribute 

var playerHealth : int;
var deadImage : Texture2D;

var score : int; 
//--------------------------------------------------
// Functions: 
//--------------------------------------------------

function Start () {
	playerHealth = 10; 
}

function Update () {
	score = gameObject.GetComponent("PlayerShooting").count; 
	if(playerHealth <= 0) 
	{
		gameObject.SendMessage("SetScore", score); 
		Death(); 
	}
}

function Death()
{
	Application.LoadLevel("EndGameMenu");
}

//--------------------------------------------------
// Functions for Adding & Subtracting Health
// Called in FirstPersonController  
//--------------------------------------------------

function SubtractHealth()
{
	playerHealth--; 
}

function AddHealth() 
{
	playerHealth += 10; 
}

//--------------------------------------------------
// Display GUI Text on Screen: 
//--------------------------------------------------

/*function OnGUI()
{
	var centeredStyle = GUI.skin.GetStyle("Label"); 
	centeredStyle.alignment = TextAnchor.UpperCenter; 
	if(playerHealth <= 0)
	{
		//GUI.Label(Rect(Screen.width/2-150, Screen.height/2-150,300,300),deadImage);
		//GUI.Label(Rect(Screen.width/2-50, Screen.height/2-50,100,100),"Game Over!! You Lost!!");

	}
}*/