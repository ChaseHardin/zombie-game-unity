﻿// Zachary Hardin
using UnityEngine;
using System.Collections;

public class GunBob : MonoBehaviour {

	public float moveAmount = 1.0f;
	public float moveSpeed = 2.0f;
	public GameObject gun; 
	public float moveOnX;
	public float moveOnY;
	public Vector3 defaultPos;
	public Vector3 newGunPos;
	public bool ONOFF = false;
		
	void Start() {
		defaultPos = transform.localPosition;

		ONOFF = true;
	}

	// Update is called once per frame
	void Update () {

		if (ONOFF == true) {
			moveOnX = Input.GetAxis ("Mouse X") * Time.deltaTime * moveAmount;
			moveOnY = Input.GetAxis ("Mouse Y") * Time.deltaTime * moveAmount;
			newGunPos = new Vector3 (defaultPos.x + moveOnX, defaultPos.y + moveOnY, defaultPos.z);
			gun.transform.localPosition = Vector3.Lerp (gun.transform.localPosition, newGunPos, moveSpeed * Time.deltaTime); 
		}

		else {
			ONOFF = false;
			gun.transform.localPosition = Vector3.Lerp (gun.transform.localPosition, defaultPos, moveSpeed * Time.deltaTime); 

		}
	}
}
