﻿//Prefab Rotation Attach to prefab

#pragma strict

 var floatup;
 var floatleft;
 var floatforward;
 var up : float = 0.3;
 var down : float = 0.3;
 
 function Start(){
     floatup = false;
     floatleft = false;
 }
 function Update(){
    
    // UP/DOWN
    if(floatup)
        floatingup();
    else if(!floatup)
        floatingdown();
}

 //----------------------------------------------
 //------------------UP/DOWN---------------------
 //----------------------------------------------
 function floatingup(){
     transform.position.y += up * Time.deltaTime;
     yield WaitForSeconds(1);
     floatup = false;
 }
 
  function floatingdown(){
     transform.position.y -= down * Time.deltaTime;
     yield WaitForSeconds(1);
     floatup = true;
 }
 