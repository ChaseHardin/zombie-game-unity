﻿//Player Controller - Attach to Player
using UnityEngine;
using System.Collections;

public class EndGameFate : MonoBehaviour 
{
	public GUIText countText;
	public GUIText winText;
	public GUIText timerText;
	public GUIText endCommand;
	public float timer = 60.0f;
	private int count;
	private Collider temp;
	private string fate;
	private int prefabCount = 3;
	bool playerLocation = false;
	bool z = false;
	string textMesssage = "Get to the Eiffel Tower!!";
	int score; 
	void Start ()
	{

		count = 0;
		SetCountText ();
		winText.text = "";
	}
	
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.tag == "Radio") 
		{
			other.gameObject.SetActive(false);    
			count = count + 1;
			SetCountText ();
		}
		
		temp = other;
	}
	
	void SetCountText ()
	{
		countText.text = "Count: " + count.ToString();
	}
	
	void Update() 
	{
		score = gameObject.GetComponent<PlayerShooting>().count; 

		if(count >= prefabCount) 
		{
			//Start Clock Timer
			if (timer >= 0){
				timer -= Time.deltaTime; 
			}
			
			//Tell the Player where to go.
			if (timer > 0) {
				endCommand.text = textMesssage;
			}
			
			//If player reaches the end location
			if (temp.gameObject.tag == "WinBox") {
				z = true;
			}
			
			if (z == true){
				endCommand.text = "";
			}
			
			/*if (temp.gameObject.tag == "LoseBox") {
                endCommand.text = textMesssage;
            }*/
			
			if ((count == prefabCount) && (temp.gameObject.tag == "WinBox")) {
				playerLocation = true;
			}
			
			
			if ((timer <= 0) && (playerLocation == false)){
				Debug.Log ("Please work 12/03/14");
				timer = 0;
				endCommand.text = "";
				//fate = "YOU LOSE!";
				//winText.text = fate;
				Application.LoadLevel("EndGameMenu");
			}
			
			if ((playerLocation == false) && (timer == 0)) {
				Application.LoadLevel("EndGameMenu");
			}
			
			if (playerLocation == true)
			{
				gameObject.SendMessage("SetScore", score); 

				Application.LoadLevel("GameOverWin");
			}
		}
		
	}
	
	void OnGUI()
	{
		if (count >= prefabCount)
		{
			timerText.text = "Count Down: " + timer.ToString("0");
		}
	}
}