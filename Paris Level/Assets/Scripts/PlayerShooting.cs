﻿
// Zachary Hardin
using UnityEngine;
using System.Collections;

public sealed class PlayerShooting : MonoBehaviour {

//--------------------------------------------------
// Declared Variables: 
//--------------------------------------------------

	public float fireRate = 0.5f;
	private float coolDown = 0.0f;
	public float damage = 25.0f; 
	public int count = 0;
	public int ammo; 
	private int bullets = 50; 
	public int ammoPack; 
	public GUIText countText; //Count Zombie Kills (Score):
	public GUIText ammoText; 
	public float interval; 

//--------------------------------------------------
// Functions: 
//--------------------------------------------------
	void Start(){
		//Updates TextGUI.
		SetCountText ();
		//InvokeRepeating ("FlashLabel", 0, interval);
		ammo = bullets; 
		ammoPack = 10; 

	}
	
	// Update is called once per frame
	void Update () {
		coolDown -= Time.deltaTime;
		SetAmmoText (); 
		if (Input.GetButton("Fire1")) {
			Fire ();

			if(ammo > 0)
			{
				ammo--; 
			}
		}

		if ((ammo == 0) && (ammoPack > 0)) {
			ammoPack--; 
			ammo = bullets; 
		}
	}
	
	void Fire() {
		if(ammo > 0 )
		{
			if (coolDown > 0) {
				return;
			}
			
			Debug.Log ("Firing Our Gun!");
			
			Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
			Transform hitTransform;
			Vector3 hitPoint;
			
			hitTransform = FindClosetHitObject (ray, out hitPoint);
			
			if (hitTransform != null) {
				Debug.Log ("We Hit " + hitTransform.transform.name);
				Health h = hitTransform.transform.GetComponent<Health>();
				
				// KEEP FOR NOW!!!
				//    while (h == null && hitTransform.parent) {
				//        hitTransform = hitTransform.parent; 
				//        h = hitTransform.transform.GetComponent<Health>();
				//    }
				
				if (h != null){
					h.takeDamage( damage, this );
				}
			}
			coolDown = fireRate;
		}
		else
		{
			Debug.Log("Out of Ammo"); 
		}
	}
	
	Transform FindClosetHitObject(Ray ray, out Vector3 hitPoint){
		RaycastHit[] hits = Physics.RaycastAll (ray);
		Transform closestHit = null;
		float distance = 0;
		hitPoint = Vector3.zero;
		
		foreach (RaycastHit hit in hits) {
			if (hit.transform != this.transform && (closestHit==null) || hit.distance < distance) {
				closestHit = hit.transform;
				distance = hit.distance;
				hitPoint = hit.point;
			}
		}
		return closestHit;
	}

//--------------------------------------------------
// Add Ammo Message sent from AmmoScript 
//--------------------------------------------------
	void AddAmmo()
	{
		ammo += 100; 
	}
	
//--------------------------------------------------
// Display GUI Text on Screen: 
//--------------------------------------------------

	public void SetCountText () {
		countText.text = "Zombie Kills: " + count.ToString ();
	}

	public void SetAmmoText()
	{
		ammoText.text = "Ammo: " + ammo.ToString () + "/" + ammoPack.ToString(); 
	}


}