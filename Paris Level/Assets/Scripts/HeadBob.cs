﻿// Zachary Hardin
using UnityEngine;
using System.Collections;

public class HeadBob : MonoBehaviour {

	public float headBobSpeed = 1;
	public float headBobStepCounter;
	public Vector3 parentLastPos;
	public float headBobAmountX = 1;
	public float headBobAmountY = 1;
	public float eyeHeightRatio = 0.9f;

	void Awake(){
				parentLastPos = transform.parent.position;
		}

	// Update is called once per frame
	void Update () {

		if (transform.parent.GetComponent<FirstPersonController>().cc.isGrounded){
			headBobStepCounter += Vector3.Distance(parentLastPos, transform.parent.position) * headBobSpeed;
		}

		Vector3 temp = transform.localPosition;

		temp.x = Mathf.Sin(headBobStepCounter) * headBobAmountX;
		temp.y = (Mathf.Cos(headBobStepCounter * 2) * headBobAmountY * -1) + (transform.parent.localScale.y * eyeHeightRatio) - (transform.parent.localScale.y / 2);

		transform.localPosition = temp;

		parentLastPos = transform.parent.position;

	}
}


