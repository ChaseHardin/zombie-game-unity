﻿// Zachary Hardin
using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	
	public float hitPoints = 100.0f;
	float currentHitPoints;
	public int count;
	public GameObject die;
	public bool dietrue = false; 
	public int dieCount = 0; 
	public NavMeshAgent agent; 
	
	// --- Grabs Static Zombie Count Variable in FirstPersonController --- 
	private GameObject findPlayer; 
	
	// Use this for initialization
	void Start () {
		count = 0;
		currentHitPoints = hitPoints;
		findPlayer = GameObject.Find("Player"); 
		agent = gameObject.GetComponent<NavMeshAgent> ();
	}
	
	public void takeDamage (float hit, PlayerShooting a){
		currentHitPoints -= hit;
		
		if(currentHitPoints <= 0) {
			Die(a);
		}
	}

	IEnumerator MyMethod(PlayerShooting a) {
		yield return new WaitForSeconds(3);
		Destroy (gameObject);

		a.count++;
		a.SetCountText ();
		findPlayer.SendMessage("DecreaseZombie"); 
	}


	void Die(PlayerShooting a) {
		gameObject.SendMessage ("navMeshOff");
		die.animation.Play ("die");
		Destroy (gameObject.collider);
		
		StartCoroutine(MyMethod (a));
		dietrue = true; 
		dieCount = 1; 
		gameObject.SendMessage ("setTrue"); 
		gameObject.SendMessage ("SetZombieDead");
		agent.speed = 0;
	}    
}