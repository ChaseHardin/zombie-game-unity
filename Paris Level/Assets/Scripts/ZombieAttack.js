﻿#pragma strict

var attack : GameObject;
var walk : GameObject;
var getHealth : boolean; 


function Start(){
    walk.animation.wrapMode = WrapMode.Loop;
    
    getHealth = false; 
    
    if(walk.animation.Play("skinny_run") == true)
    {    
    	//walk.animation.Play("skinny_run");
    }
    
    else
    {
        walk.animation.Play("walk");
    }
}

    function setTrue()
    {
        getHealth = true;     
    }


function OnTriggerEnter (col : Collider){
    if (col.gameObject.tag == "Player"){
        if(getHealth)
        {
        	attack.animation.wrapMode = WrapMode.Loop;
        	attack.animation.CrossFadeQueued("attack", 0.3, QueueMode.PlayNow);
        }
    }
}

function OnTriggerExit (col : Collider) {
    walk.animation.wrapMode = WrapMode.Loop;
    
     if(walk.animation.Play("skinny_run") == true)
     {    
     	//walk.animation.Play("skinny_run");
     }
    
    else
    {
        walk.animation.Play("walk");
    }
}