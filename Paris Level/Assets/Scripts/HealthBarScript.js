﻿#pragma strict 

var backgroundTexture : Texture;
var foregroundTexture : Texture;
var frameTexture : Texture;

var backgroundWidth: int = 80; 
var healthWidth: int = 80;
var healthHeight: int = 15;
 
var healthMarginLeft: int = 15;
var healthMarginTop: int = 10;
 
var frameWidth : int = 200;
var frameHeight: int = 15;
 
var frameMarginLeft : int = -25;
var frameMarginTop: int = 10;

//--------------------------------------------------------------------
// OnGUI() draws HealthBar to Screen
//--------------------------------------------------------------------   
function OnGUI(){
	GUI.DrawTexture( Rect(healthMarginLeft,healthMarginTop, healthMarginLeft + backgroundWidth, healthMarginTop + healthHeight), 
    backgroundTexture, ScaleMode.ScaleToFit, true, 0 );
    
    GUI.DrawTexture( Rect(healthMarginLeft,healthMarginTop, healthMarginLeft + healthWidth, healthMarginTop + healthHeight), 
	foregroundTexture, ScaleMode.ScaleAndCrop, true, 0 );
	
    GUI.DrawTexture( Rect(frameMarginLeft,frameMarginTop, frameMarginLeft + frameWidth, frameMarginTop + frameHeight), 
    frameTexture, ScaleMode.ScaleToFit, true, 0 );

}

//--------------------------------------------------------------------
// ReduceHealthWidth() is called by Enemy Zombies 
// AddHealthWidth() is called by FirstAidPack
//--------------------------------------------------------------------

function ReduceHealthWidth() 
{
	healthWidth = healthWidth - 2; 
	Debug.Log("Reduce health width"); 
}
function AddHealthWidth() 
{
	Debug.Log("Add health width"); 
	if(healthWidth < 60)
	{
		healthWidth = healthWidth + 20; 
	}
	
	if ((healthWidth > 60) && (healthWidth < 80))
	{ 
		healthWidth = 80; 
	}
}