﻿var zombiePrefab : GameObject; // Prefab to Spawn
//var zombiesSpawned : int; // Zombies spawned 
var player : GameObject; //player 
var spawn : boolean; 
var minWait : int; // Random time for spawning
var maxWait : int; 
var waitTime : int;  

// --- Variables to Access Static Zombie Count in FirstPersonController --- 
var findPlayer : GameObject; 
var staticZombieCount : int; 

var zombieLimit : int; // Sets limit of Zombies on Playing field at a time 

//Zombie List: 
var zombieArrayList  = new ArrayList(); 
var zombieList1 : GameObject;
var zombieList2 : GameObject; 
var zombieList3 : GameObject;

//-------------------------------------------------------------------------------------------------
//  Functions: 
//-------------------------------------------------------------------------------------------------

function Start () {
	minWait = 1; 
	maxWait = 10; 
	waitTime = Random.Range(minWait, maxWait); //Random Range for spawning 
	spawn = true; 
	
	zombieLimit = 50; 
	
	// --- Grabs Static Zombie Count Variable in FirstPersonController --- 
	findPlayer = GameObject.Find("Player");  
	
	zombieArrayList.Add(zombieList1); 
	zombieArrayList.Add(zombieList2);
	zombieArrayList.Add(zombieList3); 
		
}

function Update () {
    staticZombieCount = findPlayer.GetComponent("FirstPersonController").ZombieCount; 

	var ArrayListPosition : int; 
	ArrayListPosition = Random.Range(0, 3); 
	
	if((spawn) && ( staticZombieCount < zombieLimit))
	{
		Spawn(ArrayListPosition);
	}		
}

function Spawn(ArrayListPosition : int) 
{
	//Instantiate(zombieArrayList[2], transform.position, transform.rotation);  //spawn at spawner location
	
	//Debug.Log("ArrayListPosition = "+ArrayListPosition); 
	Instantiate(zombieArrayList[ArrayListPosition], transform.position, transform.rotation);  

	//zombiesSpawned +=1; 
	NewWaitTime(); 
	spawn = false; 
	SetSpawn(); 
	findPlayer.SendMessage("AddZombie"); 

}

function SetSpawn()
{
	yield WaitForSeconds(waitTime); 
	spawn = true; 
}

function NewWaitTime() 
{
	waitTime = Random.Range(minWait, maxWait); //Random Range for spawning 

}