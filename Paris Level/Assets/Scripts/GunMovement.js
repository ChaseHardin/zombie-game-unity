﻿//FROM UNITY GAME ENGINE!!
#pragma strict

public var MovementSpeed : float = 1.0; 
public var MovementAmount : float = 1.0; 
public var Gun : GameObject;
public var MovementX : float;
public var MovementY : float;
public var NewGunPosition : Vector3; 
public var DefaultPosition : Vector3;


function Start () 
{
DefaultPosition = transform.localPosition;
}

function Update () 
{
MovementX = Input.GetAxis("Mouse X") * Time.deltaTime * MovementAmount;
MovementY = Input.GetAxis("Mouse Y") * Time.deltaTime * MovementAmount;

NewGunPosition = new Vector3 (DefaultPosition.x + MovementX, DefaultPosition.y + MovementY, DefaultPosition.z);
Gun.transform.localPosition = Vector3.Lerp(Gun.transform.localPosition, NewGunPosition, MovementSpeed * Time.deltaTime);

}