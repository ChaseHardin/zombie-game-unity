﻿// Zachary Hardin.
var target : Transform;
var myTransform : Transform;  

function Awake() {
    myTransform = transform; 
}

function Start () {
    target = GameObject.FindWithTag("Player").transform; 
}

function Update(){
    GetComponent(NavMeshAgent).destination = target.position;
}
