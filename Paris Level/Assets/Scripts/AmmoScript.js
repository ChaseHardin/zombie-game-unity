﻿var player : GameObject; 
var speed : float = 30f;
var waitTime : int = 3;
var findAmmoSpawn : GameObject; 

function Start()
{
    player = GameObject.Find("Player");
    findAmmoSpawn = GameObject.FindGameObjectWithTag("AmmoSpawn"); 
    //Debug.Log(findAmmoSpawn); 
}

function Update () 
{
    transform.Rotate(Vector3.right, speed * Time.deltaTime); 
}

function OnTriggerEnter(other : Collider) {
    if(other.tag == "Player") 
    {
        player.SendMessage("AddAmmo");
        Destroy(gameObject);
        findAmmoSpawn.SendMessage("Spawn"); 
    }
}