﻿var speed : float = 30f;
var waitTime : int;
var gone : boolean = false;
var firstAidPrefab : GameObject; 

function Start()
{   
	waitTime = 20;  
    Instantiate(firstAidPrefab, transform.position, transform.rotation);
}

function Update () 
{    
    if(gone == true) 
    {
        Spawn(); 
    }
}
  
 function Spawn() 
{
	Debug.Log("Are we in FirstAidLog"); 
    yield WaitForSeconds(waitTime); 
    Instantiate(firstAidPrefab, transform.position, transform.rotation);  
}
