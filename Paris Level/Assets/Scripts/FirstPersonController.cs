﻿// Zachary Hardin
// Spenser Added Static ZombieCount & ZombieCount methods 10/29/14

using UnityEngine;
using System.Collections;

public class FirstPersonController : MonoBehaviour {
	
	// --- Player Speed ---
	public float actualMovementSpeed = 5.0f;
	public float mouseSensitivity = 3.0f;
	public float jumpSpeed = 5.0f;
	public CharacterController cc;

	// --- Tracks Zombie on Playing Field ---
	public static int ZombieCount; 

	// --- Rotation Variables ---
	float verticalRotation = 0;
	public float upDownRange = 60.0f;
	float verticalVelocity = 0;

//-----------------------------------
// Use this for initialization
//-----------------------------------
	void Start () {
		Screen.lockCursor = true;	
	}

//-----------------------------------
// Update is called once per frame
//-----------------------------------
	void Update () {
		cc = GetComponent<CharacterController> ();

		//Debug.Log (ZombieCount); 
		// --- Player Rotation (Left, Right). ---
		float rotLeftRight = Input.GetAxis ("Mouse X") * mouseSensitivity;
		transform.Rotate (0, rotLeftRight, 0);

		verticalRotation -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		verticalRotation = Mathf.Clamp (verticalRotation, -upDownRange, upDownRange);

		// --- Player Movement. ---
		float forwardSpeed = Input.GetAxis ("Vertical") * actualMovementSpeed;
		float sideSpeed = Input.GetAxis ("Horizontal") * actualMovementSpeed;
		Camera.main.transform.localRotation = Quaternion.Euler (verticalRotation, 0, 0);

		verticalVelocity += Physics.gravity.y * Time.deltaTime;

		if ( cc.isGrounded && Input.GetButtonDown ("Jump")) {
			verticalVelocity = jumpSpeed;
				}

		if (Input.GetKeyDown ("g")) 
		{
			animation.CrossFade ("toss_grenade");
		}

		Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
		speed = transform.rotation * speed;
		cc.Move (speed * Time.deltaTime);
	}

//-----------------------------------
// Adds or Subtracts Number of Zombies on Playing Field 
//-----------------------------------
	void AddZombie()
	{
		ZombieCount++; 
		//Debug.Log("Add to zombieCount "+ ZombieCount);
		
	}
	void DecreaseZombie()
	{
		ZombieCount--; 
		//Debug.Log("Decrease Zombie Count: " + ZombieCount);
	}

}