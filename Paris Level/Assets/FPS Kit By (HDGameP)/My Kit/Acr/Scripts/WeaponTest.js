﻿var drawAnim : String = "Draw";
var fireLeftAnim : String = "Fire";
var reloadAnim : String = "Reload";
var animationGO : GameObject;

var GunShot : GameObject;
var EmptyGun : GameObject; 
var ChangeWeapon : GameObject;
var ReloadGun : GameObject;
var ReloadMagazine : GameObject;



private var framesBeforeNextShot : int = 2;
private var currentShotFrame : int = 0;
var player : GameObject;
var playerAmmoCount : int;


private var drawWeapon : boolean = false;
private var reloading : boolean = false;
 
function Start (){
DrawWeapon();
WaitForSound();
player = GameObject.Find("Player");
}
 
function Update (){
 
 	playerAmmoCount = player.GetComponent("PlayerShooting").ammo;
 	
 	//Debug.Log("PlayerAmmoCount = "+playerAmmoCount);
 
 	if(playerAmmoCount > 0) {
    
	    if(Input.GetButton ("Fire1") && reloading == false && drawWeapon == false){
	        
	        if(currentShotFrame == 0){
		        var gunShot : GameObject = Instantiate(GunShot, this.transform.position, this.transform.rotation) as GameObject; 
		        gunShot.transform.parent = this.transform;
		        currentShotFrame = framesBeforeNextShot;
		        animation.Play("fire");
		       // Fire();
	        }
	        
	        else {
	        	currentShotFrame--;
	        }
	        
	        Fire();
	        }
       
       }
       
       if(playerAmmoCount <= 0){
       		if(Input.GetButtonDown ("Fire1") && reloading == false && drawWeapon == false){
       			var emptyGun : GameObject = Instantiate(EmptyGun, this.transform.position, this.transform.rotation) as GameObject; 
       			animation.Play("fire");
       		}
       }
       
        if (Input.GetKeyDown ("r") && reloading == false && drawWeapon == false){
        	Reloading();
             	 
        	ReloadSound(); 
        }
       
        /*This was for switching weapons... we don't need it anymore.
        if (Input.GetKeyDown ("1") && reloading == false){
        	DrawWeapon();
        	
        	WaitForSound();
        	
        	//var changeWeapon : GameObject = Instantiate(ChangeWeapon, this.transform.position, this.transform.rotation) as GameObject; 
        }      */
}
 
	function WaitForSound() {
		yield WaitForSeconds(0.5);
		var changeWeapon : GameObject = Instantiate(ChangeWeapon, this.transform.position, this.transform.rotation) as GameObject; 
	}

	function ReloadSound() {
		//Grab Magazine.
		yield WaitForSeconds(0.5);
		var reloadMagazine : GameObject = Instantiate(ReloadMagazine, this.transform.position, this.transform.rotation) as GameObject; 
		
		//Remove magazine
		yield WaitForSeconds(1.10);
		var reloadGun : GameObject = Instantiate(ReloadGun, this.transform.position, this.transform.rotation) as GameObject; 
		
		//Cock Rifle
		yield WaitForSeconds(0.53);
		var changeWeapon : GameObject = Instantiate(ChangeWeapon, this.transform.position, this.transform.rotation) as GameObject; 
		
		
	} 
 
 
function Fire(){
    animationGO.animation.CrossFadeQueued(fireLeftAnim, 0.08, QueueMode.PlayNow);
}
 
function DrawWeapon() {
  if(drawWeapon)
    return;
       
        animationGO.animation.Play(drawAnim);
        drawWeapon = true;
        yield WaitForSeconds(0.6);
        drawWeapon = false;
}
 
function Reloading(){
     if(reloading) return;
   
        animationGO.animation.Play(reloadAnim);
        reloading = true;
        yield WaitForSeconds(2.0);
        reloading = false;
}